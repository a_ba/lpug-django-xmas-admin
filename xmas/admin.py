from django.contrib import admin
from .models import Geschenk, Person, Wunsch


# https://docs.djangoproject.com/en/4.0/ref/contrib/admin/

# 1) Out of the Box integration - Ohne Admin-seitiges Customizing
# admin.site.register(Person)
# admin.site.register(Geschenk)
# admin.site.register(Wunsch)


# 2) ModelAdmin Objekte

class WunschAdmin(admin.ModelAdmin):
    pass


class GeschenkAdmin(admin.ModelAdmin):
    pass


class PersonAdmin(admin.ModelAdmin):
    pass


# admin.site.register(Person, PersonAdmin)
# admin.site.register(Geschenk, GeschenkAdmin)
# admin.site.register(Wunsch, WunschAdmin)

class CustomPersonAdmin(admin.ModelAdmin):
    """
    Steuerung anzuzeigender Felder
    """
    # exclude = ("secret_field",)  # Negativselektion von Feldern. Positivselektion analog über fields = [...]
    # readonly_fields = ("geschenke_count",)  # Read-only Feld für berechnetes Feld aus Model
    # filter_horizontal = ('geschenke',)  # "Besseres" many-to-many widget
    pass


# admin.site.register(Person, CustomPersonAdmin)


# 3) Fieldsets
class FieldSetPersonAdmin(admin.ModelAdmin):
    """
    PersonAdmin mit Fieldsets - erlauben bessere Strukturierung (Layout)
    """
    fieldsets = (
        ("Persönliche Informationen", {
            'fields': (("name", "vorname"), "geburtstag")  # Nested-Tuple für gleiche Zeile
        }),
        ("Kontakt", {
            'fields': ("email",)
        }),
        ("Geschenke", {
            'fields': ("geschenke",)
        }),
        ("Weitere Informationen", {
            'classes': ('collapse',),
            'fields': ("secret_field",)
        }),
    )


# admin.site.register(Person, FieldSetPersonAdmin)


# 4) Model Inlines
class WunschInLine(admin.StackedInline):
    """
    Inline Model für Wunsch: Kompaktere Alternative über TabularInline
    """
    model = Wunsch


class InlinePersonAdmin(admin.ModelAdmin):
    """
    PersonAdmin mit Inline für Wünsche
    """
    inlines = [
        WunschInLine,
    ]


# admin.site.register(Person, InlinePersonAdmin)

# 5) List-Display
class ListDisplayPersonAdmin(admin.ModelAdmin):
    """
    List-Display Konfiguration - Steuert Felder in Übersichtsseite
    """
    # list_display = ("name", "vorname", "geschenke_count")  # Anzuzeigende Felder
    # ordering = ('-name',)  # Sortierung, hier absteigend alphabetisch
    # search_fields = ('name', 'vorname')  # Durchsuchbare Felder
    pass
# admin.site.register(Person, ListDisplayPersonAdmin)
