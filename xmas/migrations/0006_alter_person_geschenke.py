# Generated by Django 4.0 on 2021-12-11 11:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('xmas', '0005_rename_lieferbar_geschenk_verfuegbar'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='geschenke',
            field=models.ManyToManyField(help_text='Zugewiesene Geschenke', related_name='Person', to='xmas.Geschenk'),
        ),
    ]
