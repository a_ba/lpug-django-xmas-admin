from django.db import models


# https://docs.djangoproject.com/en/4.0/ref/models/fields/


class Geschenk(models.Model):
    """
    Verwaltung verfügbarer Geschenke. Geschenk-Person n:m
    """

    class Kategorie(models.TextChoices):
        ELEKTRONIK = "E", "Eleketronik"
        KLEIDUNG = "K", "Kleidung"
        SPIELZEUG = "S", "Spielzeug"
        ANDERE = "A", "Andere"

    name = models.CharField(help_text='Name des Geschenks', max_length=100, null=False)
    preis = models.IntegerField(help_text='Preis in Euro', null=False)
    verfuegbar = models.BooleanField(help_text="Befindet sich auf Lager", null=False)
    kategorie = models.CharField(
        max_length=1,
        choices=Kategorie.choices,
        default=Kategorie.ANDERE,
    )

    # def __str__(self):
    #     return f"{self.name}"  # Default Repräsentation
    #
    # class Meta:
    #     verbose_name = "Geschenk"  # Name einer Instanz
    #     verbose_name_plural = "Geschenke"  # Plural-Name für Admin-Übersicht


class Person(models.Model):
    """
    Zu beschenkende Person
    """
    vorname = models.CharField(help_text='Vorname', max_length=100, null=False)
    name = models.CharField(help_text='Nachname', max_length=100, null=False)
    geburtstag = models.DateField(help_text='Geburtsdatum', null=False)
    email = models.EmailField(help_text='E-Mail Adresse', blank=True)
    geschenke = models.ManyToManyField(Geschenk, help_text="Zugewiesene Geschenke", related_name="Person", blank=True)
    secret_field = models.CharField(default="TOP SECRET", max_length=20, null=False)

    # def __str__(self):
    #     return f"{self.name}, {self.vorname}"  # Default Repräsentation

    def geschenke_count(self):
        return Geschenk.objects.filter(Person=self).count()

    # class Meta:
    #     verbose_name = "Person"  # Name einer Instanz
    #     verbose_name_plural = "Personen"  # Plural-Name für Admin-Übersicht


class Wunsch(models.Model):
    """
    Wunschzettel. Wunschzettel-Person n:1
    """
    text = models.CharField(help_text='Freitextfeld für Wünsche', max_length=200, null=False)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)

    # def __str__(self):
        # return f"{self.text[:10]}, {self.person}"  # Default Repräsentation

    # class Meta:
    #     verbose_name = "Wunsch"  # Name einer Instanz
    #     verbose_name_plural = "Wünsche"  # Plural-Name für Admin-Übersicht
