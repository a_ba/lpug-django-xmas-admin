# Agenda

Django-Projekt zu Python Meetup vom 14. Dez. 2022 https://www.meetup.com/de-DE/Leipzig-Python-User-Group/events/

[Link zur Präsentation](https://gitlab.com/a_ba/lpug-django-xmas-admin/-/blob/master/pr%C3%A4sentation/pr%C3%A4sentation.pdf)

- Aktivierung Django-Admin für Models
- Admin Customizing
    - Anpassung anzuzeigender Felder inkl. Layout (Include/Exclue, Widgets, FieldSets)
    - Integration verknüpfter Modelle (Model InLines)
    - Konfiguration Übersichtsseite (List-Display)
- Nutzerverwaltung
- Dokumentation

# Installation / Getting Started

~~~
pip install -r requirements.txt

python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
~~~